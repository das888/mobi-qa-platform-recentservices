package com.mobitv.qa.platform.recentservices.recents;

import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import junit.framework.Assert;

import org.apache.log4j.Logger;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;

enum ColumnTypes
{
	LONG, DOUBLE, STRING, BOOLEAN, UUID
}

public class CassandraUtils {

	private Cluster cluster;
	private Properties config;
	private String username;
	private String password;
	private StringBuilder hosts;
	
	public Session session;
	
	Logger log = Logger.getLogger(this.getClass());	
	static final long NUM_100NS_INTERVALS_SINCE_UUID_EPOCH = 0x01b21dd213814000L;
	
	public CassandraUtils(Properties configProp)
	{
		config = configProp;
		username = config.getProperty("cassandra_user");
	    password = config.getProperty("cassandra_password");	
	    hosts = new StringBuilder(config.getProperty("cassandra_hosts"));
	}
	
	private void getSession(String keySpace)
	{
		if(session == null)
		{
			if(cluster == null)
			{
				log.info("Intialising cluster");
				
				Assert.assertNotNull(" Missing value for property cassandra_user in  *-test-config.properties file", username);
				Assert.assertFalse(" Missing value for property cassandra_user in  *-test-config.properties file", username.isEmpty());
				Assert.assertNotNull(" Missing value for property cassandra_password in  *-test-config.properties file", password);
				Assert.assertFalse(" Missing value for property cassandra_password in  *-test-config.properties file", password.isEmpty());
				Assert.assertNotNull(" Missing value for property cassandra_hosts in  *-test-config.properties file", hosts);
				Assert.assertFalse(" Missing value for property cassandra_hosts in  *-test-config.properties file", hosts.length()<1);
			
				if(hosts.lastIndexOf(";") == -1)
				{
					cluster = Cluster.builder()                                                    // (1)
							.addContactPoint(hosts.toString())
							.withCredentials(username, password)
							.build();
				}
				else
				{
					// Provide list of hosts in valid format
					// e.g. cassandra_hosts=127.0.0.1;127.0.0.2
					cluster = Cluster.builder()                                                    // (1)
							.addContactPoints(hosts.toString().split(";"))
							.withCredentials(username, username)
							.build();
				}
				
				log.info("Cluster initialization completed");
			}
		
			if(keySpace != null)
			{
				session = cluster.connect(keySpace);
				log.info("Created session successfully for KeySpace:"+keySpace);
			}
			else
			{
				session = cluster.connect();
				log.info("Created session successfully without any KeySpace");
			}			
		}
		else
		{
			if(keySpace != null)
			{
				if(session.getLoggedKeyspace() == null | !(session.getLoggedKeyspace().equalsIgnoreCase(keySpace)))
				{
					closeSession();
					session = cluster.connect(keySpace);
					log.info("Created session successfully for KeySpace:"+keySpace);
				}
				else
				{
					log.info("Using already created session for KeySpace: "+session.getLoggedKeyspace());
				}
			}
			else
			{
				if(session.getLoggedKeyspace() != null)
				{
					closeSession();
					session = cluster.connect();
					log.info("Created session successfully without KeySpace");
				}
				else
				{
					log.info("Using already created session without KeySpace");
				}
			}
		}
	}
	
	private void closeSession()
	{
		if(session != null)
		{
			log.info("Closing session");
			session.close();
			session = null;
			log.info("Closed session successfully");
		}
		else
		{
			log.info("Session is already in CLOSE state");
		}
	}
	
	public void closeConnection()
	{
		if(cluster != null)
		{
			log.info("Closing opened session if any");
			//Closing session
			closeSession();
			
			log.info("Closing connection");
			cluster.close();
			log.info("Connection closed");
		}
		else
		{
			log.info("Connection already closed");
		}
	}
	
	public ResultSet executeQuery(String keySpace,String query, Map<String,Object> mapOfParamValues)
	{
		ResultSet resultSet = null;
		
		Assert.assertNotNull("CQL query must not be null", query);
		
		//Creating session
		getSession(keySpace);
		
		if(mapOfParamValues != null)
		{			
			//Executing query with query parameter replacement
			log.info("Executing query:"+query+" for values:"+mapOfParamValues.keySet());
			resultSet = session.execute(query,mapOfParamValues);
		}
		else
		{
			//Executing query without query parameter replacement
			log.info("Executing query:"+query);
			resultSet = session.execute(query);
		}
		
		Assert.assertNotNull("ResultSet must not be null", resultSet);
		
		return resultSet;
	}
	
	public static long getTimeFromUUID(UUID uuid) {
	    return (uuid.timestamp() - NUM_100NS_INTERVALS_SINCE_UUID_EPOCH) / 10000;
	  }
}
