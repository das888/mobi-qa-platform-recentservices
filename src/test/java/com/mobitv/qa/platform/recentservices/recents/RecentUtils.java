package com.mobitv.qa.platform.recentservices.recents;

import static com.mobitv.qa.platform.util.Constants.USER_AGENT;

import java.io.StringReader;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import junit.framework.Assert;

import com.mobitv.MobiTestBase;
import com.mobitv.commons.restclient.parser.DomParser;
import com.mobitv.commons.restclient.webclient.RestRequest;
import com.mobitv.commons.restclient.webclient.RestRequest.HttpMethodType;
import com.mobitv.exceptionwrapper.dto.ErrorWrapper;
import com.mobitv.exceptionwrapper.exception.MobiException;
import com.mobitv.json.JSONException;
import com.mobitv.json.JSONObject;
import com.mobitv.qa.platform.oauth.Token;
import com.mobitv.qa.platform.oauth.TokenData;
import com.sun.jersey.api.client.ClientResponse;

public class RecentUtils extends MobiTestBase
{	
	public String carrier, product, version, frontend;
	private TokenData tokenData = new TokenData();
	private Token token0 = new Token();
	protected JAXBContext context = null;
	
	public RecentUtils() 
	{
		carrier = config.getProperty("CARRIER");
		product = config.getProperty("PRODUCT");
		version = config.getProperty("VERSION");
		frontend = config.getProperty("QA_PLAT_FE");		
	}
	
	public String buildStringFromObject(Object object, Class<?> objectClass)
	{		
		//Build context
		try {
			context = JAXBContext.newInstance(objectClass);
		} catch (JAXBException e) {
			e.printStackTrace();
		}		
		
		StringWriter sw = new StringWriter();
			
		try 
		{
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(object, sw);
		} 
		catch (JAXBException exception) 
		{
				log.error("Error while converting object to string . Jaxb conversion Fails",	exception);
		}
		return sw.toString();
	}
	
	public Object buildObjectFromString(String strData, Class<?> objectClass)
	{		
		//Build context
		try {
			context = JAXBContext.newInstance(objectClass);
		} catch (JAXBException e) {
			e.printStackTrace();
		}		
		
		Object object = null;
		StringReader reader = new StringReader(strData);
		try 
		{
			Unmarshaller unmarshaller = context.createUnmarshaller();
			object = unmarshaller.unmarshal(reader);
		} 
		catch (JAXBException exception) 
		{
				log.error("Error while converting string to object. Jaxb conversion Fails",	exception);
		}
		return object;
	}
	
	public String replaceURLPathParams(String url, List<String> orderedParamList)
	{	
		try
		{
			url = MessageFormat.format(url, orderedParamList.toArray());		
			return url;
		}
		catch(IllegalArgumentException iaEX)
		{
			log.debug("Following error occour while URL reformatting: "+iaEX.getMessage());
		}
		finally
		{
			return null;
		}
	}
	
	/**
	 * Generate AccessToken By Username and Password
	 * 
	 * @param carrier
	 * @param product
	 * @param version
	 * @param uName
	 * @param pwd
	 * @return
	 * @throws JSONException
	 */
	public void getAccessTokenByUserNamePasswd(String carrier, String product,
			String version, String uName, String pwd, String vuid) throws JSONException {

		String host = config.getProperty("QA_PLAT_FE");
		String url = "http://{host}/identity/v5/oauth2/{carrier}/{product}/{version}/tokens.json";		
		
		carrier = (carrier != null) ? carrier : this.carrier; // "mobitv"
		product = (product != null) ? product : this.product; // "reference"
		version = (version != null) ? version : this.version; // "5.0"		
		
		String scope = "role_verified_identity";

		JSONObject json = new JSONObject();
		json.put("grant_type", "password");
		json.put("username", uName);
		json.put("password", pwd);
		json.put("client_id", config.getProperty("client_id"));
		json.put("scope", "["+scope+"]");
		String body = json.toString();
		

		try {
			RestRequest request = new RestRequest();

			request = request
					.setUrl(url)
					.setMethod(HttpMethodType.POST)
					.setContentType("application/json")					
					.setPathParam("host", host)
					.setPathParam("carrier", carrier)
					.setPathParam("product", product)
					.setPathParam("version", version)
					.setBody(body)
					.setThrowEnabled(false);
			
			if(vuid != null){
				request = request.setHeaderParam("x-mobitv-vuid", vuid);
			}
			
			ClientResponse response = request.execute();

			log.debug(response.getClientResponseStatus());
			log.debug(request.getHeaderParams().toString());
			log.debug(request.getBody());
			log.debug(response);

			if (response.getStatus() == 200) {
				DomParser tokenParser = new DomParser();
				tokenParser.setResponse(response);
								
				token0.setAccessToken(getAccessToken(tokenParser));
				token0.setExpiresIn(getExpiresIn(tokenParser));
				token0.setRefreshToken(getRefreshToken(tokenParser));
				
				List<Token> tokenList = new ArrayList<Token>();
				tokenList.add(0, token0);
				tokenData.setToken(tokenList);				
			}

		} catch (Exception e) {
			String msg = "Exception thrown in api: " + e.getMessage();
			log.error(msg);
		}

	}
	
	/**
	 * @param domParser
	 * @return
	 */
	private String getExpiresIn(DomParser domParser) {
		return domParser.evaluate("//expires_in");
	}

	/**
	 * @param domParser
	 * @return
	 */
	private String getAccessToken(DomParser domParser) {
		return domParser.evaluate("//access_token");
	}

	/**
	 * @param domParser
	 * @return
	 */
	private String getRefreshToken(DomParser domParser) {
		return domParser.evaluate("//refresh_token");
	}
	
	public Token getToken0() {
		return token0;
	}
	
	/**
	 * getProfileIdWithAccessTkn
	 * 
	 * @param authToken
	 * @param carrier
	 * @param product
	 * @param version
	 * @return profileId
	 */
	public String getProfileIdWithAccessTkn(String authToken, String carrier,
			String product, String version) 
	{
		
		authToken = "Bearer " + authToken;

		String url = "http://{host}/core/v5/session/{carrier}/{product}/{version}/current/profile.json";
		RestRequest request = new RestRequest();
		ClientResponse response = request.setUrl(url)
				.setMethod(HttpMethodType.GET)
				.setPathParam("host", config.getProperty("QA_PLAT_FE"))
				.setPathParam("carrier", carrier)
				.setPathParam("product", product)
				.setPathParam("version", version)
				.setHeaderParam("Authorization", authToken)
				.setContentType("application/json").setThrowEnabled(true)
				.execute();

		DomParser domParser = new DomParser();
		domParser.setResponse(response);
		log.debug("Get Profile ID response" + domParser.getResponseValue());
		String profileId = domParser.evaluate("//profile_id");

		return profileId;

	}
	
	/**
	 * valid10DigitVuid
	 * 
	 * @return
	 * @throws Exception
	 */
	public String valid10DigitVuid() throws Exception {
		String vuid = gen();
		if (vuid.startsWith("1")) {
			Thread.sleep(1000);
			return valid10DigitVuid();
		} else if (vuid.startsWith("911")) {
			Thread.sleep(1000);
			return valid10DigitVuid();
		} else {
			return vuid;
		}
	}
	
	/**
	 * get random value
	 * 
	 * @return
	 */
	public String gen() {
		Random r = new Random(System.currentTimeMillis());
		String ran = new Integer(Math.abs(1000000000 + r.nextInt(2000000000)))
				.toString();
		return ran;
	}
	
	/**
	 * getAccessTokenByGatewayHeader
	 * 
	 * @param carrier
	 * @param product
	 * @param version
	 * @param vuid
	 * @return AccessToken
	 */
	public String getAccessTokenByGatewayHeader(String carrier,
			String product, String version, String vuid) {

		String xWapProfile = config.getProperty("x-wap-profile");
		String userAgent = config.getProperty("user-agent");
		String contentType = "application/json";

		String body = "{\"grant_type\" : \"gateway_header\", \"client_id\" : \""
				+ config.getProperty("client_id")
				+ "\" , \"scope\" :[\"role_verified_identity\"]}";

		try {
			RestRequest request = new RestRequest();

			request = request
					.setUrl("http://{host}/identity/v5/oauth2/{carrier}/{product}/{version}/tokens.json")
					.setMethod(HttpMethodType.POST).setContentType(contentType)
					.setPathParam("host", config.getProperty("QA_PLAT_FE"))
					.setPathParam("carrier", carrier)
					.setPathParam("product", product)
					.setPathParam("version", version)
					
					.setHeaderParam("x-wap-profile", xWapProfile)
					.setHeaderParam("User-Agent", userAgent)
					.setHeaderParam("X-Mobitv-Reqid", "1234567890")
					.setThrowEnabled(false);
			if(carrier.equalsIgnoreCase("tmobile"))
				request.setHeaderParam("msisdn", vuid);
			else if(carrier.equalsIgnoreCase("boost"))
				request.setHeaderParam("clientid", vuid);
			else
				request.setHeaderParam("x-mobitv-vuid", vuid);
			
			request.setBody(body);					
			ClientResponse response = request.execute();

			log.debug(response.getClientResponseStatus());
			log.debug(request.getHeaderParams().toString());
			log.debug(request.getBody());
			log.debug(response);

			if (response.getStatus() == 200) {
				DomParser tokenParser = new DomParser();
				tokenParser.setResponse(response);
				
				token0.setAccessToken(getAccessToken(tokenParser));
				token0.setExpiresIn(getExpiresIn(tokenParser));
				token0.setRefreshToken(getRefreshToken(tokenParser));
				
				List<Token> tokenList = new ArrayList<Token>();
				tokenList.add(0, token0);
				tokenData.setToken(tokenList);
				
				String accessToken = tokenParser.evaluate("//access_token");
				
				return accessToken;
			}
		} catch (Exception e) {
			String msg = "Exception thrown in api: " + e.getMessage();
			log.error(msg);
		}

		return null;
	}
	
	/**
	 * getAccessTokenByGatewayHeader
	 * 
	 * @param carrier
	 * @param product
	 * @param version
	 * @param vuid
	 * @return AccessToken
	 */
	public String getEncryptStrForAccessTkn(String randomStr) {
		
		try {
			
			String url = "http://{host}:{port}/mobi-stub-sprint-ws/stub/encrypt";
			RestRequest request = new RestRequest();

			request = request.setUrl(url)
					.setMethod(HttpMethodType.GET)
					.setPathParam("host", config.getProperty("QA_PLAT_MSP"))
					.setPathParam("port", config.getProperty("QA_PLAT_MSP_PORT"))
					.setHeaderParam("x-mobitv-vuid", randomStr)
					.setContentType("text/plain").setThrowEnabled(true);
			
			log.debug("request is :"+request);
			
			ClientResponse response = request.execute();

			log.debug(response.getClientResponseStatus());
			log.debug(request.getHeaderParams().toString());
			log.debug(request.getBody());

			if (response.getStatus() == 200) {
				String output = response.getEntity(String.class);  
				System.out.println("\n============getResponse============");  
				System.out.println("Done "+output);  
				
				return output;
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		return null;
	}
	
	/**
	 * getAccessTokenByGatewayHeader
	 * 
	 * @param carrier
	 * @param product
	 * @param version
	 * @param vuid
	 * @return AccessToken
	 */
	public String getAccessTknOnEcryptStr(String carrier,
			String product, String version, String vuid, String encryptStr) {

		String xWapProfile = config.getProperty("x-wap-profile");
		String userAgent = config.getProperty("user-agent");
		String contentType = "application/json";

		String body = "{\"grant_type\" : \"gateway_header\", \"client_id\" : \""
				+ config.getProperty("client_id")
				+ "\" , \"scope\" :[\"role_verified_identity\"]}";

		try {
			RestRequest request = new RestRequest();

			request = request
					.setUrl("http://{host}/identity/v5/oauth2/{carrier}/{product}/{version}/tokens.json")
					.setMethod(HttpMethodType.POST).setContentType(contentType)
					.setPathParam("host", config.getProperty("QA_PLAT_FE"))
					.setPathParam("carrier", carrier)
					.setPathParam("product", product)
					.setPathParam("version", version)
					.setHeaderParam("clientid", encryptStr)
					.setHeaderParam("x-wap-profile", xWapProfile)
					.setHeaderParam("User-Agent", userAgent)
//					.setHeaderParam("x-mobitv-vuid", vuid)
					.setHeaderParam("X-Mobitv-Reqid", vuid)
					.setBody(body)
					.setThrowEnabled(false);
					
			ClientResponse response = request.execute();

			log.debug(response.getClientResponseStatus());
			log.debug(request.getHeaderParams().toString());
			log.debug(request.getBody());
			log.debug(response);

			if (response.getStatus() == 200) {
				DomParser tokenParser = new DomParser();
				tokenParser.setResponse(response);
				
				token0.setAccessToken(getAccessToken(tokenParser));
				token0.setExpiresIn(getExpiresIn(tokenParser));
				token0.setRefreshToken(getRefreshToken(tokenParser));
				
				List<Token> tokenList = new ArrayList<Token>();
				tokenList.add(0, token0);
				tokenData.setToken(tokenList);
				
				String accessToken = tokenParser.evaluate("//access_token");
				
				return accessToken;
			}
		} catch (Exception e) {
			String msg = "Exception thrown in api: " + e.getMessage();
			log.error(msg);
		}

		return null;
	}
	
	public Object getAPIResponse(String APIUrl, HttpMethodType methodType, String respFormat, Map<String,Object> headerParameters, Map<String,Object> pathParameters, String bodyContent, Map<String,Object> queryParameters, String bodyContentType, Class<? extends Object> respClass, int responseCode, boolean shouldReturnException, String apiName)
			throws Exception
{
		RestRequest request = new RestRequest();
		ClientResponse response = null;
			
		String contentType = (bodyContentType == null) ? "application/json" : bodyContentType;
		String url = null;
		
		Assert.assertNotNull("API URL must not be null", APIUrl);
		log.info("Response format: "+respFormat);
		
		url = (respFormat == null) ? APIUrl : APIUrl + "." + respFormat; 
					
		request.setUrl(url);
		
		if(pathParameters != null)
		{
			log.info("Setting path parameters");
			for(Entry<String,Object> entity:pathParameters.entrySet())
			{
				log.info("Parameter: "+entity.getKey()+" Value: "+entity.getValue());
				request.setPathParam(entity.getKey(),entity.getValue());
			}
		}
		
		url = request.getUrl();
		
		if(headerParameters != null)
		{
			log.info("Setting header parameters");
			for(Entry<String,Object> entity:headerParameters.entrySet())
			{
				log.info("Parameter: "+entity.getKey()+" Value: "+entity.getValue());
				request.setHeaderParam(entity.getKey(),entity.getValue());
			}
		}
				
		if(queryParameters != null)
		{
			url+="?";
			log.info("Setting query parameters");
			for(Entry<String,Object> queryParam:queryParameters.entrySet())
			{
				log.info("Parameter: "+queryParam.getKey()+" Value: "+queryParam.getValue());
				if(url.endsWith("?"))
				{
					url +=queryParam.getKey()+"="+ queryParam.getValue();
				}
				else
				{
					url +="&"+queryParam.getKey()+"="+ queryParam.getValue();
				}
			}
		}
		
		if(bodyContent != null)
		{
			log.info("Request body: "+bodyContent);
			request.setBody(bodyContent);
		}
			
		try
		{
			response = request
						.setUrl(url)
						.setMethod(methodType)
						.setHeaderParam("User-Agent", USER_AGENT)
						.setContentType(contentType)
						.setAccept("application/json")
						.setThrowEnabled(false).execute();
			log.info(apiName+" API response: " + response);
							
			if(response.getStatus() != responseCode)
			{
				Assert.assertTrue("Response code doesn't match. \nDetails:"+response.getEntity(String.class), response.getStatus() == responseCode);
			}
				
			if(respClass != null)
			{
				return response.getEntity(respClass);
			}
			else
			{
				return response;
			}
		}
		catch(MobiException mobiEx)
		{
			log.info("Mobi exception occured. \n Error details: \n 1.Error code:"+mobiEx.getHttpStatusCode()+" \n 2.Error description:"+mobiEx.getLocalizedMessage());
			if(shouldReturnException)
			{
				ErrorWrapper errWrapper = (ErrorWrapper)response.getEntity(ErrorWrapper.class);
				return errWrapper;
			}
			else
			{
				throw mobiEx;
			}
		}
}


}
