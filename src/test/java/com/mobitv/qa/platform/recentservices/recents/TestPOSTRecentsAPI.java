package com.mobitv.qa.platform.recentservices.recents;

import static com.mobitv.qa.platform.util.Constants.USER_AGENT;
import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertNotNull;
import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.UUID;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.junit.Assert;
import org.junit.Test;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.utils.UUIDs;
import com.mobitv.commons.restclient.webclient.RestRequest.HttpMethodType;
import com.mobitv.platform.core.dto.RecentList;
import com.mobitv.platform.core.dto.RecentListItem;
import com.mobitv.qa.platform.util.SolrHelper;


public class TestPOSTRecentsAPI extends RecentUtils
{
	//Utility classes variables
	private CassandraUtils cassandraUtil = new CassandraUtils(config);
	private SolrHelper solrHelper = new SolrHelper(config);
	
	private String grantType = config.getProperty("grant_type");
	private String username = null;
	private String pwd = null;
	private String vuid = null;
	private String profileId = null;
	private String accessToken = null;
	private String ranmdomStr = null;
	private String encryptStr = null;
	
	private String ref_type;
	private String category;
	private int currentStreamPosition;
	private int duration;
	private int statusCode;
	
	private String keySpace;
	private String dbQuery; 
	private UUID timeInUUID;
	
	Map<Integer,Map<String, Map<ColumnTypes, String>>> mapOfValuesToVerify;
	Map<String, Object> mapOfCQLAttributeValues;
	
	public void setUp() throws Exception
	{
		duration = (int)System.currentTimeMillis()/100000;
		currentStreamPosition = duration-1000;
		
		//Setting up RESTAssured parameters
		RestAssured.baseURI = "http://"+config.getProperty("QA_PLAT_PRF");
		RestAssured.port = Integer.parseInt(config.getProperty("QA_PLAT_PRF_PORT"));
		RestAssured.defaultParser = Parser.JSON;
		
		if("password".equalsIgnoreCase(grantType)){		
			
			username = config.getProperty("username");
			pwd = config.getProperty("password");				
			vuid = config.getProperty("username");
			
			assertNotNull(username);
			assertNotNull(pwd);
							
			getAccessTokenByUserNamePasswd(carrier, product,
					version, username, pwd, vuid);			
			accessToken = getToken0().getAccessToken();			
			profileId = getProfileIdWithAccessTkn(accessToken, carrier, product, version);
						
		} else {

			if ("carrier_gateway".equalsIgnoreCase(grantType)
					&& !carrier.equalsIgnoreCase("sprint")) {
				vuid = valid10DigitVuid();
				log.debug("vuid: " + vuid);

				accessToken = getAccessTokenByGatewayHeader(carrier,
						product, version, vuid);
				profileId = getProfileIdWithAccessTkn(accessToken,
						carrier, product, version);
				
				log.info("SetUp: access token :: " + accessToken);					
			} else {
				ranmdomStr = RandomStringUtils.randomAlphabetic(13);
				ranmdomStr = ranmdomStr + "@sprintpcs.com";
				encryptStr = getEncryptStrForAccessTkn(ranmdomStr);
				vuid = encryptStr;

				log.debug("vuid: " + vuid + " Random Str :" + ranmdomStr);

				accessToken = getAccessTknOnEcryptStr(carrier,
						product, version, vuid, encryptStr);
				profileId = getProfileIdWithAccessTkn(accessToken,
						carrier, product, version);
				
				log.info("setUp: access token :: " + accessToken);
			}
			Assert.assertNotNull("Access token must not be null",accessToken);
			Assert.assertNotNull("Profile ID must not be null",profileId);
		}
	}

	
	private SolrDocument getValidSolrDocID(String solrQuery)
	{
		log.info("Querying SOLR for query: "+solrQuery);
		
		SolrDocumentList docList = solrHelper.getSolrDocumentsByCriteria(solrQuery);
		
		Assert.assertNotNull("Solr document list must not be null", docList);		
		Assert.assertTrue("Returned list must contains atleast 1 document", docList.getNumFound()>0);
		
		return docList.get(0);
	}
	
	private String getValueFromRow(Row row, ColumnTypes colType, String colName)
	{
		String value=null;
		switch(colType)
		{
			case BOOLEAN:			
				value = String.valueOf(row.getString(colName));
				break;
			
			case LONG:
				value = String.valueOf(row.getLong(colName));
				break;
			
			case DOUBLE:
				value = String.valueOf(row.getDouble(colName));
				break;
				
			case UUID:
				value = String.valueOf(row.getUUID(colName));
				break;			
			
			case STRING:
				value = row.getString(colName);
				break;
			
			default:
				log.info("Invalid column type");
				break;
		}
		return value;
	}
	
	private void setDetailsIntoRecordVerificationMap(int recordNo, String colName, ColumnTypes colType, String colValue)
	{
		if(recordNo == -1)
		{
			mapOfValuesToVerify = null;
		}
		else
		{
			if(mapOfValuesToVerify == null)
			{
				mapOfValuesToVerify = new TreeMap<Integer,Map<String, Map<ColumnTypes, String>>>();
			}
			
			//Verifies whether entry for specified record number exist in map
			Map<String, Map<ColumnTypes, String>> mapOfColValue = mapOfValuesToVerify.get(recordNo);		
			if(mapOfColValue == null)
			{
				mapOfColValue = new TreeMap<String, Map<ColumnTypes, String>>();
			}
			
			//Verifies whether entry for specified column name exist in map
			Map<ColumnTypes, String> mapOfColTypeValue = mapOfColValue.get(colName);
			if(mapOfColTypeValue == null)
			{
				mapOfColTypeValue = new TreeMap<ColumnTypes, String>();
			}
		
			mapOfColTypeValue.put(colType,colValue);
			mapOfColValue.put(colName, mapOfColTypeValue);
			mapOfValuesToVerify.put(recordNo, mapOfColValue);
		}
	}
	
	private void setValueInCQLQuery(String attrName, Object value)
	{
		if(mapOfCQLAttributeValues == null)
		{
			mapOfCQLAttributeValues = new TreeMap<String,Object>();
		}
		
		if(value == null)
		{
			mapOfCQLAttributeValues.remove(attrName);
		}
		else
		{
			mapOfCQLAttributeValues.put(attrName, value);
		}
	}
	
	private void verifyCassandraDBEntries(String keyspace,String query, boolean shouldExist)
	{
		ResultSet rs = cassandraUtil.executeQuery(keyspace, query, mapOfCQLAttributeValues);		

		log.info("/**********************DB verification starts**********************/");
		if(mapOfValuesToVerify != null)
		{		
			int recordNo;
			String columnName, actualValue, expectedValue;
			ColumnTypes typeOfColumn;
			boolean foundExResult = false;
			
			//Verifying expected db content
			Iterator<Row> rowIterator = rs.iterator();
			ExWhile:while(rowIterator.hasNext())
			{
				Row row = rowIterator.next();
				for(Entry<Integer,Map<String, Map<ColumnTypes, String>>> element:mapOfValuesToVerify.entrySet())
				{
					recordNo = element.getKey();					
					log.info("Verifying entries of record no: "+recordNo);
					
					for(Entry<String, Map<ColumnTypes, String>> subElement:element.getValue().entrySet())
					{
						columnName = subElement.getKey();
						
						for(Entry<ColumnTypes, String> childElement:subElement.getValue().entrySet())
						{
							typeOfColumn = childElement.getKey();
							expectedValue = childElement.getValue();
							actualValue = getValueFromRow(row,typeOfColumn,columnName);
							
							if(shouldExist)
							{
								log.info("Column: "+columnName+" Expected value: "+expectedValue+" Actual value: "+actualValue);
							}
							else
							{
								log.info("Column: "+columnName+" Unexpected value: "+expectedValue+" Actual value: "+actualValue);
							}
							
							if(typeOfColumn.equals(ColumnTypes.UUID))
							{
								UUID actualUUID = UUID.fromString(actualValue);
								UUID expectedUUID = UUID.fromString(expectedValue);
								
								Date actualDate = new Date(CassandraUtils.getTimeFromUUID(actualUUID));
								Date expectedDate = new Date(CassandraUtils.getTimeFromUUID(expectedUUID));
								
								// Finding difference between two dates
							    long diff = actualDate.getTime() - expectedDate.getTime();
							    long diffSeconds = diff / 1000 % 60;
							    long diffMinutes = diff / (60 * 1000) % 60;
							    long diffHours = diff / (60 * 60 * 1000);
							    int diffInDays = (int) (diff / (1000 * 60 * 60 * 24));
							    
							    log.info(">>--->>Difference [Actual vs Expected] In Days:"+diffInDays+" In [Hours:"+diffHours+",Minutes:"+diffMinutes+",Seconds:"+diffSeconds+"]");
								
								foundExResult = (diffInDays==0) ? ((diffHours==0) ? ((diffMinutes==0) ? ((diffSeconds<60) ? true : false) : false) : false) : false;
							}
							else
							{
								if(columnName.equalsIgnoreCase("countOfRows"))
								{
									if(Integer.valueOf(expectedValue) == 0)
									{
										foundExResult = (Integer.valueOf(actualValue)==Integer.valueOf(expectedValue)) ? true : false;
									}
									else
									{
										foundExResult = (Integer.valueOf(actualValue)>=Integer.valueOf(expectedValue)) ? true : false;
									}
								}
								else
								{
									foundExResult = (actualValue.equalsIgnoreCase(expectedValue)) ? true : false;
								}
							}
						}
						if(!foundExResult)
						{
							continue ExWhile;
						}
					}
					if(foundExResult)
					{
						break ExWhile;
					}
				}				
			}
		
			if(shouldExist)
			{
				Assert.assertTrue("Expected values not found", foundExResult);
				log.info("Found expected record");
			}
			else
			{
				Assert.assertFalse("Unexpected values must not exist", foundExResult);
				log.info("Missing unexpected record");
			}
		}
		else
		{
			log.info("Cassandra DB verification not required");
		}
		log.info("/**********************DB verification finished**********************/");
	}
	
	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * UDM_PLAT_Mobi_Recents_Service_Post_Recents_001
	 * UDM_PLAT_Mobi_Recents_Service_Post_Recents_008
	 * PLAT_Mobi_Recents_Service_GetRecent_001
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557567
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557574
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557488
	 *  
	 * @Summary
	 * Verify that channel can be added to the recents
	 * Verify that multiple recent entries can be created for one channel using POST recent API
	 * Verify Get Recents API should return response successfully with 200 status code
	 */
	
	@Test
	public void testUDM_PLAT_Mobi_Recents_Service_Post_Recents_001_008() throws Exception
	{
		String solrQuery = "type_ssi:channel AND offer_ids_ssmi:* AND has_program_data_bsi:true";

		//Fetching valid channel doc
		ref_type = "channel";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for channel: "+doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>)(doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size()==0 ? "comedy" : String.valueOf(listOfCategories.get(0)));	
		
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+currentStreamPosition+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type+"\"}]}";
		
		timeInUUID = UUIDs.timeBased();
		
		int actualStatus = given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).getStatusCode();
		
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(currentStreamPosition));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));	
		
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);
		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");

		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type= ?";
		setValueInCQLQuery("ref_type", ref_type);

		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG, "1");

		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));

		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");

		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		setValueInCQLQuery("profile_id", null);

		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/*****************************************************************************/

		RestAssured.defaultParser = Parser.XML;
		statusCode = 200;
		RecentList response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("ref_type", ref_type)
				.header("accept", "application/xml").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);
        
		List<RecentListItem> recentListItems = response.getRecentlistItems();
		
		log.info("[Total recent items] "+recentListItems);
	}
	
	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * UDM_PLAT_Mobi_Recents_Service_Post_Recents_002
	 * PLAT_Mobi_Recents_Service_GetRecent_003
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557568
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557490
	 *  
	 * @Summary
	 * Verify that catchup program can be added to the recents : PAST program with catchup enabled
	 * Verify Get Recents API by ref_type as program
	 */
	
	@Test
	public void testUDM_PLAT_Mobi_Recents_Service_Post_Recents_002() throws Exception
	{
		String solrQuery = "type_ssi:program AND offer_ids_ssmi:* AND is_catchup_enabled_ssmi:\""+carrier+"-"+product+"-"+version+"-rest::true\" AND end_time_dsi: [* TO NOW/MINUTE]";
		
		//Fetching valid channel doc
		ref_type = "program";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for program: "+doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>)(doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size()==0 ? "comedy" : String.valueOf(listOfCategories.get(0)));	
		
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+currentStreamPosition+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type+"\"}]}";
		log.info("Request Body: "+data);
		
		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).getStatusCode();
		
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(currentStreamPosition));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));	
		
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");

		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type= ?";
		setValueInCQLQuery("ref_type", ref_type);
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		setValueInCQLQuery("profile_id", null);

		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/*****************************************************************************/
		
		//GET Recent
		RestAssured.defaultParser = Parser.XML;
		statusCode = 200;
		RecentList response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("ref_type", ref_type)
				.header("accept", "application/xml").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);

		List<RecentListItem> recentListItems = response.getRecentlistItems();
		
		log.info("Verifying list of recent items for ref_type query parameter");
		Iterator<RecentListItem> listIterator = recentListItems.iterator();
		while(listIterator.hasNext())
		{
			RecentListItem item = listIterator.next();
			
			log.info("Ref ID: "+item.getRefId()+" Ref Type: "+item.getRefType());
			Assert.assertTrue("API must return items with ref_type: "+ref_type, item.getRefType().equalsIgnoreCase(ref_type));
			if(item.getRefId().equalsIgnoreCase(String.valueOf(doc.getFieldValue("uuid"))))
			{
				log.info("Found recently placed item ref_id:"+doc.getFieldValue("uuid"));
			}
		}
		log.info("/******************Verification completed******************/");
	}
	
	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * UDM_PLAT_Mobi_Recents_Service_Post_Recents_003
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557569
	 *  
	 * @Summary
	 * Verify that program can be added to the recents : CURRENTLY RUNNING program with catchup disabled
	 */
	
	@Test
	public void testUDM_PLAT_Mobi_Recents_Service_Post_Recents_003() throws Exception
	{
		String solrQuery = "type_ssi:program AND offer_ids_ssmi:* AND is_catchup_enabled_ssmi:\""+carrier+"-"+product+"-"+version+"-rest::false\" AND start_time_dsi: [NOW TO *]";
		
		//Fetching valid channel doc
		ref_type = "program";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for program: "+doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>)(doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size()==0 ? "comedy" : String.valueOf(listOfCategories.get(0)));	
				
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
				
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+currentStreamPosition+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type+"\"}]}";
		log.info("Request Body: "+data);
				
		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().
				pathParam("carrier", carrier).
				pathParam("product", product).
				pathParam("version", version).
				pathParam("profileId", profileId).
				contentType("application/json").
				body(data).
				header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
				when(). 
				post(config.getProperty("post.recents.v1")).getStatusCode();
				
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(currentStreamPosition));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));	
		
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");

		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type= ?";
		setValueInCQLQuery("ref_type", ref_type);
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		setValueInCQLQuery("profile_id", null);

		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/*****************************************************************************/
	}
	
	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * UDM_PLAT_Mobi_Recents_Service_Post_Recents_004
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557570
	 *  
	 * @Summary
	 * Verify that VOD can be added to the recents
	 */
	@Test
	public void testUDM_PLAT_Mobi_Recents_Service_Post_Recents_004() throws Exception
	{
		String solrQuery = "type_ssi:vod AND offer_ids_ssmi:*";
		
		//Fetching valid channel doc
		ref_type = "vod";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for vod: "
				+ doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>) (doc
				.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size() == 0 ? "comedy"
				: String.valueOf(listOfCategories.get(0)));

		// Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header")) ? grantType
				: "gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;

		// POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\"" + currentStreamPosition
				+ "\"," + "\"duration\":\"" + duration + "\","
				+ "\"category\":\"" + category + "\"," + "\"ref_id\":\""
				+ doc.getFieldValue("uuid") + "\"," + "\"ref_type\":\""
				+ ref_type + "\"}]}";
		log.info("Request Body: " + data);

		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.contentType("application/json").body(data)
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.post(config.getProperty("post.recents.v1")).getStatusCode();

		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode,
				actualStatus);

		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(currentStreamPosition));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));	
		
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");

		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type= ?";
		setValueInCQLQuery("ref_type", ref_type);
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		setValueInCQLQuery("profile_id", null);

		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/*****************************************************************************/
	}
	
	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * UDM_PLAT_Mobi_Recents_Service_Post_Recents_005
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557571
	 *  
	 * @Summary
	 * Verify that recorded program can be added to the recents : PAST program with recording enabled
	 */
	@Test
	public void testUDM_PLAT_Mobi_Recents_Service_Post_Recents_005() throws Exception
	{
		String solrQuery = "type_ssi:program AND offer_ids_ssmi:* AND is_recording_enabled_ssmi:\""+carrier+"-"+product+"-"+version+"-rest::true\" AND end_time_dsi: [* TO NOW-1DAY]";
		
		//Fetching valid channel doc
		ref_type = "program";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for program: "
				+ doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>) (doc
				.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size() == 0 ? "comedy"
				: String.valueOf(listOfCategories.get(0)));

		// Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header")) ? grantType
				: "gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;

		// POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\"" + currentStreamPosition
				+ "\"," + "\"duration\":\"" + duration + "\","
				+ "\"category\":\"" + category + "\"," + "\"ref_id\":\""
				+ doc.getFieldValue("uuid") + "\"," + "\"ref_type\":\""
				+ ref_type + "\"}]}";
		log.info("Request Body: " + data);

		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.contentType("application/json").body(data)
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.post(config.getProperty("post.recents.v1")).getStatusCode();

		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode,
				actualStatus);

		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(currentStreamPosition));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));	
		
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");

		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type= ?";
		setValueInCQLQuery("ref_type", ref_type);
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		setValueInCQLQuery("profile_id", null);

		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/*****************************************************************************/
	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * UDM_PLAT_Mobi_Recents_Service_Post_Recents_006
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557572
	 *  
	 * @Summary
	 * Verify that series can be added to the recents
	 */
	@Test
	public void testUDM_PLAT_Mobi_Recents_Service_Post_Recents_006() throws Exception
	{
		String solrQuery = "type_ssi:series";
		
		//Fetching valid channel doc
		ref_type = "series";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for series: "
				+ doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>) (doc
				.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size() == 0 ? "comedy"
				: String.valueOf(listOfCategories.get(0)));

		// Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header")) ? grantType
				: "gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;

		// POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\"" + currentStreamPosition
				+ "\"," + "\"duration\":\"" + duration + "\","
				+ "\"category\":\"" + category + "\"," + "\"ref_id\":\""
				+ doc.getFieldValue("uuid") + "\"," + "\"ref_type\":\""
				+ ref_type + "\"}]}";
		log.info("Request Body: " + data);

		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.contentType("application/json").body(data)
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.post(config.getProperty("post.recents.v1")).getStatusCode();

		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode,
				actualStatus);

		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(currentStreamPosition));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));	
		
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");

		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type= ?";
		setValueInCQLQuery("ref_type", ref_type);
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		setValueInCQLQuery("profile_id", null);

		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/*****************************************************************************/	
	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * UDM_PLAT_Mobi_Recents_Service_Post_Recents_007
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557573
	 *  
	 * @Summary
	 * Verify that multiple items can be added to the recents
	 */
	@Test
	public void testUDM_PLAT_Mobi_Recents_Service_Post_Recents_007() throws Exception
	{
		String solrQuery = "type_ssi:series";
		
		//Fetching valid channel doc
		ref_type = "series";
		String ref_type1 = "program";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		solrQuery = "type_ssi:program";
		SolrDocument doc1 = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for program: "
				+ doc1.getFieldValue("uuid")+" and series: "+ doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>) (doc
				.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size() == 0 ? "comedy"
				: String.valueOf(listOfCategories.get(0)));

		// Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header")) ? grantType
				: "gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;

		// POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\"" + currentStreamPosition
				+ "\"," + "\"duration\":\"" + duration + "\","
				+ "\"category\":\"" + category + "\"," + "\"ref_id\":\""
				+ doc.getFieldValue("uuid") + "\"," + "\"ref_type\":\""+ref_type+"\"},"
				+ "{"
				+ "\"current_stream_position\":\"" + currentStreamPosition
				+ "\"," + "\"duration\":\"" + duration + "\","
				+ "\"category\":\"" + category + "\"," + "\"ref_id\":\""
				+ doc1.getFieldValue("uuid") + "\"," + "\"ref_type\":\""
				+ ref_type1 + "\"}]}";
		log.info("Request Body: " + data);

		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.contentType("application/json").body(data)
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.post(config.getProperty("post.recents.v1")).getStatusCode();

		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode,
				actualStatus);
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(currentStreamPosition));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));

			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//2
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc1.getFieldValue("uuid")));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"2");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"','"+ref_type1+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//2
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", doc1.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
				
			//1
		setValueInCQLQuery("profile_id",null);
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
				
			//2
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", doc1.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * UDM_PLAT_Mobi_Recents_Service_Post_Recents_009
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557575
	 *  
	 * @Summary
	 * Verify that recent-id & created_time values sent in the POST recents request does not get populated in cassandra
	 */
	@Test
	public void testUDM_PLAT_Mobi_Recents_Service_Post_Recents_009() throws Exception
	{
		String solrQuery = "type_ssi:channel AND offer_ids_ssmi:* AND has_program_data_bsi:true";
		
		//Fetching valid channel doc
		ref_type = "channel";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for channel: "	+ doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>) (doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size() == 0 ? "comedy"
				: String.valueOf(listOfCategories.get(0)));

		// Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header")) ? grantType : "gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;

		// POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\"" + currentStreamPosition
				+ "\"," + "\"duration\":\"" + duration + "\","
				+ "\"category\":\"" + category + "\"," + "\"ref_id\":\""
				+ doc.getFieldValue("uuid") + "\"," + "\"ref_type\":\""+ref_type+"\",\"recent_id\":\"1111\",\"created_time\":\"9fccda70-cd4c-11e6-9598-0800200c9a66\"}]}";
		log.info("Request Body: " + data);

		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.contentType("application/json").body(data)
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.post(config.getProperty("post.recents.v1")).getStatusCode();

		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode,
				actualStatus);
		
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, "9fccda70-cd4c-11e6-9598-0800200c9a66");
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(currentStreamPosition));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));

		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, false);
	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * UDM_PLAT_Mobi_Recents_Service_Post_Recents_011
	 * UDM_PLAT_Mobi_Recents_Service_Post_Recents_012
	 * UDM_PLAT_Mobi_Recents_Service_Post_Recents_013
	 * UDM_PLAT_Mobi_Recents_Service_Post_Recents_014
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557577
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557578
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557579
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557580
	 *  
	 * @Summary
	 * Verify that POST Recents API should return 403 Forbidden when profile-id & access-token used does not match
	 * Verify that POST Recents API should return 403 Forbidden when access-token is not passed in the request
	 * Verify that POST Recents API returns 400 Bad Request when mandatory parameters ref_type OR ref_id is missing in the request
	 * Verify that POST Recents API returns 400 Bad Request when current_stream_position sent in the request is greater than duration
	 */
	@Test
	public void testUDM_PLAT_Mobi_Recents_Service_Post_Recents_011_012_013_014() throws Exception
	{
		String solrQuery = "type_ssi:channel AND offer_ids_ssmi:* AND has_program_data_bsi:true";
		
		//Fetching valid channel doc
		ref_type = "channel";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for channel: "	+ doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>) (doc
				.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size() == 0 ? "comedy"
				: String.valueOf(listOfCategories.get(0)));

		// Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header")) ? grantType
				: "gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;

		// POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\"" + currentStreamPosition
				+ "\"," + "\"duration\":\"" + duration + "\","
				+ "\"category\":\"" + category + "\"," + "\"ref_id\":\""
				+ doc.getFieldValue("uuid") + "\"," + "\"ref_type\":\""+ref_type+"\"}]}";
		log.info("Request Body: " + data);

		timeInUUID = UUIDs.timeBased();
		log.info("Querying POST with Invalid Profile ID");
		Response response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", "12345")
				.contentType("application/json").body(data)
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.post(config.getProperty("post.recents.v1")).andReturn();
		statusCode = 403;
		log.info("Response : "+response.asString());
		Assert.assertEquals("Status code not as expected", statusCode,
				response.getStatusCode());
		
		log.info("Querying POST without access token");
		response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.contentType("application/json").body(data)
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.post(config.getProperty("post.recents.v1")).andReturn();

		statusCode = 403;
		log.info("Response : "+response.asString());
		Assert.assertEquals("Status code not as expected", statusCode,
				response.getStatusCode());
		
		data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\"" + currentStreamPosition
				+ "\"," + "\"duration\":\"" + duration + "\","
				+ "\"category\":\"" + category + "\"," + "\"ref_id\":\""
				+ doc.getFieldValue("uuid") + "\"}]}";
		log.info("Request Body: " + data);
		
		setUp();
		authToken = "Bearer " + accessToken;
		log.info("Querying POST without ref_type in request body");
		response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.contentType("application/json").body(data)
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.post(config.getProperty("post.recents.v1")).andReturn();

		statusCode = 400;
		log.info("Response : "+response.asString());
		Assert.assertEquals("Status code not as expected", statusCode,
				response.getStatusCode());
		
		data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\"" + currentStreamPosition
				+ "\"," + "\"duration\":\"" + duration + "\","
				+ "\"category\":\"" + category + "\"," + "\"ref_type\":\""+ref_type+"\"}]}";
		log.info("Request Body: " + data);
		
		setUp();
		authToken = "Bearer " + accessToken;
		log.info("Querying POST without ref_type in request body");
		response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.contentType("application/json").body(data)
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.post(config.getProperty("post.recents.v1")).andReturn();

		statusCode = 400;
		log.info("Response : "+response.asString());
		Assert.assertEquals("Status code not as expected", statusCode,
				response.getStatusCode());
		//response.

	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * PLAT_Mobi_Recents_Service_GetRecent_004
	 * PLAT_Mobi_Recents_Service_GetRecent_005
	 * PLAT_Mobi_Recents_Service_GetRecent_006
	 * PLAT_Mobi_Recents_Service_GetRecent_007
	 * PLAT_Mobi_Recents_Service_GetRecent_014
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557491
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557492
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557493
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557494
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557501
	 *  
	 * @Summary
	 * Verify Get Recents API by ref_type as catchup
	 * Verify Get Recents API by ref_type as recording
	 * Verify Get Recents API by completed as query params in request
	 * Verify Get Recents API by category as query params in request
	 * Verify Get Recent API (ALL) that is without providing any query params
	 */
	
	@Test
	public void testPLAT_Mobi_Recents_Service_GetRecent_004_005_006_007_014() throws Exception
	{		
		ref_type = "catchup";
		String ref_type1 = "recording";
		String docID = "12345678";
		String docID1 = "87654321";
		log.info("Verifying POST Recents for catchup: "+docID);
		log.info("Verifying POST Recents for recording: "+docID1);
		category = "comedy";	
		
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+duration+"\"," /*Putting current_stream_position=duration so that "completed" will get populate with true*/
				+ "\"duration\":\""+duration+"\"," 
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+docID+"\","
				+ "\"ref_type\":\""+ref_type+"\"},"
				+ "{"
				+ "\"current_stream_position\":\""+currentStreamPosition+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+docID1+"\","
				+ "\"ref_type\":\""+ref_type1+"\"}]}";
		
		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).getStatusCode();
		
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
		
		/*********************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, docID);
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(duration));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));

			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
				
			//2
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(currentStreamPosition));
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, docID1);
		verifyCassandraDBEntries(keySpace, dbQuery, true);
				
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"2");
				
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"','"+ref_type1+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
				
		//Verification DB Table:profile_recents_history	
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
				
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", docID);
		verifyCassandraDBEntries(keySpace, dbQuery, true);
				
			//2
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", docID1);
		verifyCassandraDBEntries(keySpace, dbQuery, true);
				
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
						
			//1
		setValueInCQLQuery("profile_id",null);
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", docID);
		verifyCassandraDBEntries(keySpace, dbQuery, true);
						
			//2
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", docID1);
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/**********************************************************/
		
		//GET Recent for query_param: completed
		log.info("Verifying GET Recents for query_param: ref_type=catchup");
		RestAssured.defaultParser = Parser.XML;
		statusCode = 200;
		RecentList response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("ref_type", "catchup")
				.header("accept", "application/xml").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);
        
		List<RecentListItem> recentListItems = response.getRecentlistItems();
		Assert.assertTrue("List of recent items must be equal to/greater than 1", (recentListItems != null) && (recentListItems.size()>=1));
		
		log.info("Verifying expected list of items with ref_type=catchup");
		Iterator<RecentListItem> listIterator = recentListItems.iterator();
		RecentListItem item=null;
		proList:while(listIterator.hasNext())
		{
			item = listIterator.next();
			
			log.info("Ref ID: "+item.getRefId()+" Ref Type: "+item.getRefType());
			Assert.assertTrue("API must return items with ref_type: catchup", item.getRefType().equalsIgnoreCase("catchup"));
			if(item.getRefId().equalsIgnoreCase(docID))
			{
				log.info("Found item ref_id:"+docID);
				break proList;
			}
			
			item = null;
		}
		
		Assert.assertNotNull("Expected item [ref_id:"+docID+"] with ref_type=catchup not found", item);
		log.info("/******************Verification completed******************/");
		
		//GET Recent for query_param: ref_type=recording
		log.info("Verifying GET Recents for query_param: ref_type=recording");
		RestAssured.defaultParser = Parser.XML;
		statusCode = 200;
		response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("ref_type", "recording")
				.header("accept", "application/xml").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);

		recentListItems = response.getRecentlistItems();
		Assert.assertTrue("List of recent items must be equal to/greater than 1",
				(recentListItems != null) && (recentListItems.size() >= 1));

		log.info("Verifying expected list of items with ref_type=recording");
		listIterator = recentListItems.iterator();
		item = null;
		proList1: while (listIterator.hasNext()) {
			item = listIterator.next();

			log.info("Ref ID: " + item.getRefId() + " Ref Type: "
					+ item.getRefType());
			Assert.assertTrue("API must return items with ref_type: recording", item.getRefType().equalsIgnoreCase("recording"));
			if (item.getRefId().equalsIgnoreCase(docID1)) {
				log.info("Found item ref_id:" + docID1);
				break proList1;
			}

			item = null;
		}

		Assert.assertNotNull("Expected item [ref_id:" + docID
				+ "] with ref_type=recording not found", item);
		log.info("/******************Verification completed******************/");
		
		//GET Recent for query_param: completed
		log.info("Verifying GET Recents for query_param: completed");
		statusCode = 200;
		response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("completed", String.valueOf(true))
				.header("accept", "application/xml").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);

		recentListItems = response.getRecentlistItems();
		Assert.assertTrue(
				"Completed list of recent items must be equal to/greater than 1",
				(recentListItems != null) && (recentListItems.size() >= 1));

		log.info("Verifying expected list of completed items");
		listIterator = recentListItems.iterator();
		item = null;
		proList2: while (listIterator.hasNext()) {
			item = listIterator.next();

			log.info("Ref ID: " + item.getRefId() + " Ref Type: "
					+ item.getRefType());
			if (item.getRefId().equalsIgnoreCase(docID)) {
				log.info("Found completed item ref_id:" + docID);
				break proList2;
			}

			item = null;
		}

		Assert.assertNotNull("Expected completed item [ref_id:" + docID
				+ "] not found", item);
		log.info("/******************Verification completed******************/");
		
		//GET Recent for query_param: category
		log.info("Verifying GET Recents for query_param: category");
		statusCode = 200;
		response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("category", category)
				.header("accept", "application/xml").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);

		recentListItems = response.getRecentlistItems();
		Assert.assertTrue(
				"List of recent items must be equal to/greater than 2",
				(recentListItems != null) && (recentListItems.size() >= 2));

		log.info("Verifying expected list of items with category="+category);
		listIterator = recentListItems.iterator();
		item = null;
		proList2: while (listIterator.hasNext()) {
			item = listIterator.next();

			log.info("Ref ID: " + item.getRefId() + " Category: "
					+ item.getCategory());
			if (item.getRefId().equalsIgnoreCase(docID)) {
				log.info("Found item ref_id:" + docID);
				docID = "FOUND";
				if(docID1.equalsIgnoreCase("FOUND"))
				break proList2;
			}
			if (item.getRefId().equalsIgnoreCase(docID1)) {
				log.info("Found item ref_id:" + docID1);
				docID1 = "FOUND";
				
				if(docID.equalsIgnoreCase("FOUND"))
				break proList2;
			}

			item = null;
		}

		Assert.assertTrue("Expected item [ref_id:" + docID
				+ "] category=comedy not found", (docID.equalsIgnoreCase("FOUND")));
		Assert.assertTrue("Expected item [ref_id:" + docID1
				+ "] category=comedy not found", (docID1.equalsIgnoreCase("FOUND")));
		log.info("/******************Verification completed******************/");
		
		//GET Recent without any query_param
		log.info("Verifying GET Recents without any query_param");
		statusCode = 200;
		response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.header("accept", "application/xml").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);

		recentListItems = response.getRecentlistItems();
		Assert.assertTrue(
				"Completed list of recent items must be equal to/greater than 1",
				(recentListItems != null) && (recentListItems.size() >= 1));

		log.info("Verifying expected list of completed items");
		listIterator = recentListItems.iterator();
		item = null;
		long creationTimeFuture = -1;
		proList3: while (listIterator.hasNext()) {
			item = listIterator.next();

			log.info("Ref ID: " + item.getRefId() + " Creation time: "
					+ item.getCreationTime());
			
			if(creationTimeFuture != -1)
			{
				if (item.getCreationTime() > creationTimeFuture) 
				{
					log.info("Found item in invalid order");
					item = null;
					break proList3;
				}
			}
			creationTimeFuture = item.getCreationTime();
		}

		Assert.assertNotNull("Item list found in unexpected order", item);
		log.info("/******************Verification completed******************/");
	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * PLAT_Mobi_Recents_Service_GetRecent_008
	 * PLAT_Mobi_Recents_Service_GetRecent_009
	 * PLAT_Mobi_Recents_Service_GetRecent_010
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557495
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557496
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557497
	 *  
	 * @Summary
	 * Verify Get Recents API with multi-valued category as query params in request
	 * Verify Get Recents API by ref_type such that it doesn't exists in profile_recents_reftype
	 * Verify Get Recents API by category such that it doesn't exists in profile_recents_reftype
	 */
	
	@Test
	public void testPLAT_Mobi_Recents_Service_GetRecent_008_009_010() throws Exception
	{		
		//Fetching valid channel doc
		ref_type = "channel";
		String docID = "12345679";
		String docID1 = "97654321";
		log.info("Verifying POST Recents for channel: "+docID);
		category = "comedy";	
		String category1 = "music";
		
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+duration+"\"," 
				+ "\"duration\":\""+duration+"\"," 
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+docID+"\","
				+ "\"ref_type\":\""+ref_type+"\"},"
				+ "{"
				+ "\"current_stream_position\":\""+currentStreamPosition+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category1+"\","
				+ "\"ref_id\":\""+docID1+"\","
				+ "\"ref_type\":\""+ref_type+"\"}]}";
		
		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).getStatusCode();
		
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, docID);
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(duration));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));

			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//2
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, docID1);
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(currentStreamPosition));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"2");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", docID);
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//2
		setValueInCQLQuery("ref_id", docID1);
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
				
			//1
		setValueInCQLQuery("profile_id",null);
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", docID);
		verifyCassandraDBEntries(keySpace, dbQuery, true);
				
			//2
		setValueInCQLQuery("ref_id", docID1);
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//GET Recent for query_param: category (multiple values)
		log.info("Verifying GET Recents for query_param: category=comedy,music");
		RestAssured.defaultParser = Parser.XML;
		statusCode = 200;
		RecentList response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("category", "comedy,music")
				.header("accept", "application/xml").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);
        
		List<RecentListItem> recentListItems = response.getRecentlistItems();
		Assert.assertTrue("List of recent items must be equal to/greater than 2", (recentListItems != null) && (recentListItems.size()>=2));
		
		log.info("Verifying expected list of items with category=comedy,music");
		Iterator<RecentListItem> listIterator = recentListItems.iterator();
		RecentListItem item=null;
		proList:while(listIterator.hasNext())
		{
			item = listIterator.next();
			
			log.info("Ref ID: "+item.getRefId()+" Category: "+item.getCategory());
			Assert.assertTrue("API must return items with category=comedy,music", (item.getCategory().equalsIgnoreCase(category) || item.getCategory().equalsIgnoreCase("music")));
			if (item.getRefId().equalsIgnoreCase(docID)) {
				log.info("Found item ref_id:" + docID);
				docID = "FOUND";
				if(docID1.equalsIgnoreCase("FOUND"))
				break proList;
			}
			if (item.getRefId().equalsIgnoreCase(docID1)) {
				log.info("Found item ref_id:" + docID1);
				docID1 = "FOUND";
				
				if(docID.equalsIgnoreCase("FOUND"))
				break proList;
			}
			
			item = null;
		}
		
		Assert.assertTrue("Expected item [ref_id:" + docID
				+ "] category=comedy not found", (docID.equalsIgnoreCase("FOUND")));
		Assert.assertTrue("Expected item [ref_id:" + docID1
				+ "] category=music not found", (docID1.equalsIgnoreCase("FOUND")));
		log.info("/******************Verification completed******************/");
		
		//GET Recent for query_param: category (for non-existant value in profile_recents_reftype)
		log.info("Verifying GET Recents for query_param: category=sports");
		statusCode = 200;
		response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("category", "sports")
				.header("accept", "application/xml").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);

		recentListItems = response.getRecentlistItems();
		Assert.assertTrue("Size of recent items list must be 0",
				recentListItems.size() == 0);
		log.info("/******************Verification completed******************/");
		
		//GET Recent for query_param: ref_type (for non-existant value in profile_recents_reftype)
		log.info("Verifying GET Recents for query_param: ref_type=news");
		statusCode = 200;
		response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("ref_type", "news")
				.header("accept", "application/xml").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);

		recentListItems = response.getRecentlistItems();
		Assert.assertTrue(
				"Size of recent items list must be 0",
				recentListItems.size()== 0);
		log.info("/******************Verification completed******************/");
	
	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * PLAT_Mobi_Recents_Service_GetRecent_011
	 * PLAT_Mobi_Recents_Service_GetRecent_012
	 * PLAT_Mobi_Recents_Service_GetRecent_013
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557498
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557499
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557500
	 *  
	 * @Summary
	 * Verify Get Recents API by completed=false and ref_type query param such that same ref_id and ref_type is in the cassandra multiple times and only last one is completed
	 * Verify Get Recents API by completed=true and ref_type query param such that same ref_id and ref_type is in the cassandra multiple times and only last one is completed
	 * Verify Get Recents API by ref_type, category,completed as query params in request
	 */
	
	@Test
	public void testPLAT_Mobi_Recents_Service_GetRecent_011_012_013() throws Exception
	{		
		String solrQuery = "type_ssi:vod AND offer_ids_ssmi:*";

		//Fetching valid vod doc
		ref_type = "vod";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for vod: "+doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>)(doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size()==0 ? "comedy" : String.valueOf(listOfCategories.get(0)));	
				
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+duration+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type+"\"}]}";
		
		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).getStatusCode();
		
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(duration));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));

			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
				
			//1
		setValueInCQLQuery("profile_id",null);
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
				
		//GET Recent for query_param: completed=false and ref_type=vod
		log.info("Verifying GET Recents for query_param: completed=false and ref_type=vod");
		RestAssured.defaultParser = Parser.XML;
		statusCode = 200;
		RecentList response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("completed", String.valueOf(false))
				.queryParam("ref_type", ref_type)
				.header("accept", "application/xml").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);
        
		List<RecentListItem> recentListItems = response.getRecentlistItems();
		Assert.assertTrue("List of recent items must be equal to/greater than 0", (recentListItems != null) && (recentListItems.size()>=0));
		
		log.info("Verifying expected list of items");
		Iterator<RecentListItem> listIterator = recentListItems.iterator();
		RecentListItem item=null;
		while(listIterator.hasNext())
		{
			item = listIterator.next();
			
			log.info("Ref ID: "+item.getRefId()+" Category: "+item.getCategory());
			Assert.assertTrue("API must not return item with ref_id"+doc.getFieldValue("uuid"), !(item.getRefId().equalsIgnoreCase(String.valueOf(doc.getFieldValue("uuid")))));
		}
		
		log.info("/******************Verification completed******************/");
		
		//GET Recent for query_param: completed=true and ref_type=vod
		log.info("Verifying GET Recents for query_param: completed=true and ref_type=vod and category="+category);
		statusCode = 200;
		response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("completed", String.valueOf(true))
				.queryParam("ref_type", ref_type)
				.queryParam("category", category)
				.header("accept", "application/xml").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);

		recentListItems = response.getRecentlistItems();
		Assert.assertTrue(
				"List of recent items must be equal to/greater than 1",
				(recentListItems != null) && (recentListItems.size() >= 1));

		log.info("Verifying expected list of items");
		listIterator = recentListItems.iterator();
		item = null;
		proList: while (listIterator.hasNext()) {
			item = listIterator.next();

			log.info("Ref ID: " + item.getRefId() + " Completed: "
					+ item.isCompleted());
			Assert.assertTrue("API must return items with completed=true", item.isCompleted());
			if (item.getRefId().equalsIgnoreCase(
					String.valueOf(doc.getFieldValue("uuid")))) {
				log.info("Found item ref_id:" + item.getRefId());
				Assert.assertEquals(
						"API must return duration of completed vod as expected",
						item.getDuration(), String.valueOf(duration));
				break proList;
			}
			item = null;
		}

		Assert.assertTrue(
				"Expected item [ref_id:"
						+ String.valueOf(doc.getFieldValue("uuid"))
						+ "] not found", item != null);
		log.info("/******************Verification completed******************/");
		
	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * PLAT_Mobi_Recents_Service_GetRecent_015
	 * PLAT_Mobi_Recents_Service_GetRecent_017
	 * PLAT_Mobi_Recents_Service_GetRecent_018
	 * PLAT_Mobi_Recents_Service_GetRecent_019
	 * PLAT_Mobi_Recents_Service_GetRecent_020
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557502
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557504
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557505
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557506
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557507
	 *  
	 * @Summary
	 * Verify Get Recents API with invalid profile_id
	 * Verify Get Recents API with null Session Id 'x-mobitv-sid: '
	 * Verify Get Recents API with invalid Session Id 'x-mobitv-sid: xyz'
	 * Verify Get Recents API without auth header 'Authorization: Bearer '
	 * Verify Get Recents API with invalid auth header 'Authorization: Bearer blahblah'
	 */
	
	@Test
	public void testPLAT_Mobi_Recents_Service_GetRecent_015_017_to_020() throws Exception
	{		
		String solrQuery = "type_ssi:vod AND offer_ids_ssmi:*";

		//Fetching valid vod doc
		ref_type = "vod";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for vod: "+doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>)(doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size()==0 ? "comedy" : String.valueOf(listOfCategories.get(0)));	
				
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+duration+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type+"\"}]}";
		
		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).getStatusCode();
		
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(duration));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));

			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
				
			//1
		setValueInCQLQuery("profile_id",null);
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/******************************************************/
		
		//GET Recent with null details
		log.info("Verifying GET Recents without session details");
		statusCode = 200;
		given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.header("accept", "application/json").and()
				.header("x-mobitv-sid", "").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
		
		/******************************************************/
		
		//GET Recent with invalid details
		log.info("Verifying GET Recents with invalid session details");
		statusCode = 200;
		given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.header("accept", "application/json").and()
				.header("x-mobitv-sid", "xyz").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
		
		//GET Recent for invalid profile ID
		log.info("Verifying GET Recents for invalid Profile ID");
		statusCode = 403;
		given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", "1111111111111")
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
		
		//GET Recent without Authorization
		log.info("Verifying GET Recents without Authorization");
		statusCode = 403;
		given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.header("accept", "application/json").and()
				.header("Authorization", "Bearer ").and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
		
		//GET Recent with invalid Authorization
		log.info("Verifying GET Recents with invalid Authorization");
		statusCode = 403;
		given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.header("accept", "application/json").and()
				.header("Authorization", "Bearer BlahBlah").and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * PLAT_Mobi_Recents_Service_GetRecent_022
	 * PLAT_Mobi_Recents_Service_GetRecent_026
	 * PLAT_Mobi_Recents_Service_GetRecent_027
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557509
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557513
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557514
	 *  
	 * @Summary
	 * Verify Get Recents API supports pagination with respect to size
	 * Verify Get Recents API supports pagination with respect to start_time
	 * Verify Get Recents API by combination of ref_type, category,completed,size and start_time as query params in request
	 */
	
	@Test
	public void testPLAT_Mobi_Recents_Service_GetRecent_022_026() throws Exception
	{		
		String solrQuery = "type_ssi:vod AND offer_ids_ssmi:*";

		//Fetching valid vod doc
		ref_type = "vod";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for vod: "+doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>)(doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size()==0 ? "comedy" : String.valueOf(listOfCategories.get(0)));	
				
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\"4\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"0\","
				+ "\"ref_type\":\""+ref_type+"\"},"
						+ "{"
				+ "\"current_stream_position\":\"3\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"1\","
				+ "\"ref_type\":\""+ref_type+"\"},"
						+ "{"
				+ "\"current_stream_position\":\"2\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"2\","
				+ "\"ref_type\":\""+ref_type+"\"},"
						+ "{"
				+ "\"current_stream_position\":\""+duration+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"3\","
				+ "\"ref_type\":\""+ref_type+"\"},"
						+ "{"
				+ "\"current_stream_position\":\"1\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"4\","
				+ "\"ref_type\":\""+ref_type+"\"}]}";
		
		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).getStatusCode();
		
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
						
		//GET Recent with length query_param
		log.info("Verifying GET Recents with length query_param");
		RestAssured.defaultParser = Parser.XML;
		statusCode = 200;
		RecentList response = given().pathParam("carrier", carrier)
						.pathParam("product", product)
						.pathParam("version", version)
						.pathParam("profileId", profileId)
						.queryParam("length", "3")
						.header("accept", "application/xml").and()
						.header("Authorization", authToken).and()
						.header("User-Agent", USER_AGENT).when()
						.get(config.getProperty("get.recents.v1")).then()
						.statusCode(statusCode).and().extract().as(RecentList.class);

		List<RecentListItem> recentListItems = response.getRecentlistItems();
		Assert.assertTrue("Completed list of recent items must be equal to/less than 3",
						(recentListItems != null) && (recentListItems.size() <= 3));

		log.info("Verifying expected list of expected items");
		Iterator<RecentListItem>listIterator = recentListItems.iterator();
		RecentListItem item = null;
		long creationTimeFuture = -1;
		proList3: while (listIterator.hasNext()) 
			{
				item = listIterator.next();

				log.info("Ref ID: " + item.getRefId() + " Creation time: "
							+ item.getCreationTime());
					
				if(creationTimeFuture != -1)
				{
					if (item.getCreationTime() > creationTimeFuture) 
					{
						log.info("Found item in invalid order");
						item = null;
						break proList3;
					}
				}
				creationTimeFuture = item.getCreationTime();
			}

	Assert.assertNotNull("Item list found in unexpected order", item);
	log.info("/******************Verification completed******************/");
	
	//GET Recent with start_time query_param
	log.info("Verifying GET Recents with start_time query_param");
	
	response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("start_time", timeInUUID.toString())
				.header("accept", "application/xml").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);

	recentListItems = response.getRecentlistItems();
	
	if(recentListItems != null)
	{
	log.info("Verifying expected list of expected items");
	listIterator = recentListItems.iterator();
	item = null;
	creationTimeFuture = -1;
	proList3: while (listIterator.hasNext()) 
		{
			item = listIterator.next();

			log.info("Ref ID: " + item.getRefId() + " Creation time: "
						+ item.getCreationTime());
				
			if(creationTimeFuture != -1)
			{
				if (item.getCreationTime() > creationTimeFuture) 
				{
					log.info("Found item in invalid order");
					item = null;
					break proList3;
				}
			}
			creationTimeFuture = item.getCreationTime();
		}
	}

		Assert.assertNotNull("Item list found in unexpected order", item);
		log.info("/******************Verification completed******************/");
		
		//GET Recent with ref_type, category,completed,size and start_time query_params
		log.info("Verifying GET Recents with ref_type, category,completed,size and start_time query_params");
		
		response = given().pathParam("carrier", carrier)
					.pathParam("product", product)
					.pathParam("version", version)
					.pathParam("profileId", profileId)
					.queryParam("start_time", timeInUUID.toString())
					.queryParam("ref_type", ref_type)
					.queryParam("category", category)
					.queryParam("completed", String.valueOf(true))
					.queryParam("length", "3")
					.header("accept", "application/xml").and()
					.header("Authorization", authToken).and()
					.header("User-Agent", USER_AGENT).when()
					.get(config.getProperty("get.recents.v1")).then()
					.statusCode(statusCode).and().extract().as(RecentList.class);

		recentListItems = response.getRecentlistItems();
		Assert.assertTrue("Completed list of recent items must be equal to/less than 1",
				(recentListItems != null) && (recentListItems.size() <= 1));

		log.info("Verifying expected list of expected items");
		listIterator = recentListItems.iterator();
		item = listIterator.next();
		
		Assert.assertEquals("Item ref_id must match", String.valueOf(doc.getFieldValue("uuid"))+"3", item.getRefId());

	}

	
	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * UDM_PLAT_Mobi_Recents_Service_FE_Delete_Recents_001
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557586
	 *  
	 * @Summary
	 * Verify that a recent entry can be deleted using Frontend DELETE Recents API : one pair of ref_type & ref_id
	 */
	
	@Test
	public void testUDM_PLAT_Mobi_Recents_Service_FE_Delete_Recents_001() throws Exception
	{		
		String solrQuery = "type_ssi:vod AND offer_ids_ssmi:*";

		//Fetching valid vod doc
		ref_type = "vod";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for vod: "+doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>)(doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size()==0 ? "comedy" : String.valueOf(listOfCategories.get(0)));	
				
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+duration+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type+"\"}]}";
		
		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).getStatusCode();
		
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(duration));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));

			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile	
		setValueInCQLQuery("profile_id",null);
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/********************************************************/		
				
		//DELETE Recent
		log.info("Verifying DELETE Recents");
		statusCode = 204;
		given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("recent_id_list", ref_type+"|"+doc.getFirstValue("uuid"))
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.delete(config.getProperty("delete.recents.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		
			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"0");
		
		dbQuery = "select count(*) as countOfRows from recents_profile where profile_id= ? AND ref_type= ? AND ref_id= ?";
			
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * UDM_PLAT_Mobi_Recents_Service_FE_Delete_Recents_003
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557588
	 *  
	 * @Summary
	 * Verify that multiple recent entries can be deleted using Frontend DELETE Recents API: multiple pairs of ref_type & ref_id
	 */
	
	@Test
	public void testUDM_PLAT_Mobi_Recents_Service_FE_Delete_Recents_003() throws Exception
	{		
		String solrQuery = "type_ssi:vod AND offer_ids_ssmi:*";

		//Fetching valid vod doc
		ref_type = "vod";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for vod: "+doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>)(doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size()==0 ? "comedy" : String.valueOf(listOfCategories.get(0)));	
		
		//Fetching valid channel doc
		String ref_type1 = "channel";
		SolrDocument doc1 = getValidSolrDocID("type_ssi:channel AND offer_ids_ssmi:*");
		log.info("Verifying POST Recents for channel: " + doc1.getFieldValue("uuid"));
		List<Object> listOfCategories1 = (ArrayList<Object>) (doc
				.getFieldValues("genre_list_ssmi"));
		String category1 = (listOfCategories1 == null || listOfCategories1.size() == 0 ? "comedy"
				: String.valueOf(listOfCategories1.get(0)));
		
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+currentStreamPosition+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type+"\"},{"
				+ "\"current_stream_position\":\""+currentStreamPosition+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category1+"\","
				+ "\"ref_id\":\""+doc1.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type1+"\"}]}";
		
		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).getStatusCode();
		
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(currentStreamPosition));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));

			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//2
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc1.getFieldValue("uuid")));		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"2");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"','"+ref_type1+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//1
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", doc1.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		
		setValueInCQLQuery("profile_id", null);
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", doc1.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/********************************************************/		
		
		//DELETE Recent
		log.info("Verifying DELETE Recents");
		statusCode = 204;
		given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("recent_id_list", ref_type+"|"+doc.getFirstValue("uuid")+","+ref_type1+"|"+doc1.getFirstValue("uuid"))
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.delete(config.getProperty("delete.recents.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		
			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		
			//2
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc1.getFieldValue("uuid")));		
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"2");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"','"+ref_type1+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		
		//Verification DB Table:profile_recents_history	
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//1
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", doc1.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		
		dbQuery = "select count(*) as countOfRows from recents_profile where profile_id= ? AND ref_type= ? AND ref_id= ?";
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", doc1.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		/********************************************************/
	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * UDM_PLAT_Mobi_Recents_Service_FE_Delete_Recents_004
	 * UDM_PLAT_Mobi_Recents_Service_FE_Delete_Recents_005
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557589
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557590
	 *  
	 * @Summary
	 * Verify that all recent entries for a profile are deleted if NO query parameters are passed to Frontend DELETE Recents API
	 * Verify that Frontend DELETE Recents API does NOTHING when profile-id for which no recents entries are available in cassandra is passed
	 */
	
	@Test
	public void testUDM_PLAT_Mobi_Recents_Service_FE_Delete_Recents_004_005() throws Exception
	{		
		String solrQuery = "type_ssi:vod AND offer_ids_ssmi:*";

		//Fetching valid vod doc
		ref_type = "vod";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for vod: "+doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>)(doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size()==0 ? "comedy" : String.valueOf(listOfCategories.get(0)));	
		
		//Fetching valid channel doc
		String ref_type1 = "channel";
		SolrDocument doc1 = getValidSolrDocID("type_ssi:channel AND offer_ids_ssmi:*");
		log.info("Verifying POST Recents for channel: " + doc1.getFieldValue("uuid"));
		List<Object> listOfCategories1 = (ArrayList<Object>) (doc
				.getFieldValues("genre_list_ssmi"));
		String category1 = (listOfCategories1 == null || listOfCategories1.size() == 0 ? "comedy"
				: String.valueOf(listOfCategories1.get(0)));
		
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+currentStreamPosition+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type+"\"},{"
				+ "\"current_stream_position\":\""+currentStreamPosition+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category1+"\","
				+ "\"ref_id\":\""+doc1.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type1+"\"}]}";
		
		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).getStatusCode();
		
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(currentStreamPosition));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));

			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//2
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc1.getFieldValue("uuid")));		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"2");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"','"+ref_type1+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//2
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", doc1.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		
		setValueInCQLQuery("profile_id", null);
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", doc1.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/********************************************************/		
				
		//DELETE Recent
		log.info("Verifying DELETE Recents without query parameters");
		statusCode = 204;
		given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.delete(config.getProperty("delete.recents.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"0");
		
		keySpace = "personalization_recents";
		dbQuery = "select count(*) as countOfRows from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
				
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"2");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"','"+ref_type1+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ?  AND ref_type IN('"+ref_type+"','"+ref_type1+"') AND  ref_id IN('"+String.valueOf(doc.getFieldValue("uuid"))+"','"+String.valueOf(doc1.getFieldValue("uuid"))+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"0");
		
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type IN('"+ref_type+"','"+ref_type1+"') AND  ref_id IN('"+String.valueOf(doc.getFieldValue("uuid"))+"','"+String.valueOf(doc1.getFieldValue("uuid"))+"') AND profile_id= ?";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/********************************************************/		

		//DELETE Recent
		log.info("Verifying DELETE Recents without query parameters");
		statusCode = 204;
		given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.delete(config.getProperty("delete.recents.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * UDM_PLAT_Mobi_Recents_Service_Delete_Recents_001
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557581
	 *  
	 * @Summary
	 * Verify that a recent entry can be deleted using Backend DELETE Recents API : one pair of ref_type & ref_id
	 */
	
	@Test
	public void testUDM_PLAT_Mobi_Recents_Service_Delete_Recents_001() throws Exception
	{		
		String solrQuery = "type_ssi:vod AND offer_ids_ssmi:*";

		//Fetching valid vod doc
		ref_type = "vod";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for vod: "+doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>)(doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size()==0 ? "comedy" : String.valueOf(listOfCategories.get(0)));	
				
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+duration+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type+"\"}]}";
		
		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).getStatusCode();
		
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
		
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(duration));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));

			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
				
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
			
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
				
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
				
		//Verification DB Table:recents_profile	
		setValueInCQLQuery("profile_id",null);
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/********************************************************/		
		
		//DELETE Recent
		log.info("Verifying DELETE Recents");
		statusCode = 204;
		 given().queryParam("profileId", profileId)
				.queryParam("recent_id_list", ref_type+"|"+doc.getFirstValue("uuid"))
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.delete(config.getProperty("delete.backend.recents.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		
			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"0");
		
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
			
		setValueInCQLQuery("profile_id",null);
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * UDM_PLAT_Mobi_Recents_Service_Delete_Recents_003
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557583
	 *  
	 * @Summary
	 * Verify that multiple recent entries can be deleted using Backend DELETE Recents API: multiple pairs of ref_type & ref_id
	 */
	
	@Test
	public void testUDM_PLAT_Mobi_Recents_Service_Delete_Recents_003() throws Exception
	{		
		String solrQuery = "type_ssi:vod AND offer_ids_ssmi:*";

		//Fetching valid vod doc
		ref_type = "vod";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for vod: "+doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>)(doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size()==0 ? "comedy" : String.valueOf(listOfCategories.get(0)));	
		
		//Fetching valid channel doc
		String ref_type1 = "channel";
		SolrDocument doc1 = getValidSolrDocID("type_ssi:channel AND offer_ids_ssmi:*");
		log.info("Verifying POST Recents for channel: " + doc1.getFieldValue("uuid"));
		List<Object> listOfCategories1 = (ArrayList<Object>) (doc
				.getFieldValues("genre_list_ssmi"));
		String category1 = (listOfCategories1 == null || listOfCategories1.size() == 0 ? "comedy"
				: String.valueOf(listOfCategories1.get(0)));
		
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+currentStreamPosition+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type+"\"},{"
				+ "\"current_stream_position\":\""+currentStreamPosition+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category1+"\","
				+ "\"ref_id\":\""+doc1.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type1+"\"}]}";
		
		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).getStatusCode();
		
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(currentStreamPosition));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));

			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//2
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc1.getFieldValue("uuid")));		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"2");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"','"+ref_type1+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//1
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", doc1.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		
		setValueInCQLQuery("profile_id", null);
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", doc1.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/********************************************************/		
				
		//DELETE Recent
		log.info("Verifying DELETE Recents");
		statusCode = 204;
		 given().queryParam("profileId", profileId)
				.queryParam("recent_id_list", ref_type+"|"+doc.getFirstValue("uuid")+","+ref_type1+"|"+doc1.getFirstValue("uuid"))
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.delete(config.getProperty("delete.backend.recents.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		
			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		
			//2
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc1.getFieldValue("uuid")));		
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"2");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"','"+ref_type1+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		
		//Verification DB Table:profile_recents_history	
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//1
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", doc1.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		
		setValueInCQLQuery("profile_id", null);
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", doc1.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		/********************************************************/
	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * UDM_PLAT_Mobi_Recents_Service_Delete_Recents_004
	 * UDM_PLAT_Mobi_Recents_Service_Delete_Recents_005
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557584
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557585
	 *  
	 * @Summary
	 * Verify that all recent entries for a profile are deleted if recent_id_list query parameter is NOT passed to Backend DELETE Recents API
	 * Verify that Backend DELETE Recents API does NOTHING when none of the query parameters are passed
	 */
	
	@Test
	public void testUDM_PLAT_Mobi_Recents_Service_Delete_Recents_004_005() throws Exception
	{		
		String solrQuery = "type_ssi:vod AND offer_ids_ssmi:*";

		//Fetching valid vod doc
		ref_type = "vod";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for vod: "+doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>)(doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size()==0 ? "comedy" : String.valueOf(listOfCategories.get(0)));	
		
		//Fetching valid channel doc
		String ref_type1 = "channel";
		SolrDocument doc1 = getValidSolrDocID("type_ssi:channel AND offer_ids_ssmi:*");
		log.info("Verifying POST Recents for channel: " + doc1.getFieldValue("uuid"));
		List<Object> listOfCategories1 = (ArrayList<Object>) (doc
				.getFieldValues("genre_list_ssmi"));
		String category1 = (listOfCategories1 == null || listOfCategories1.size() == 0 ? "comedy"
				: String.valueOf(listOfCategories1.get(0)));
		
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+currentStreamPosition+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type+"\"},{"
				+ "\"current_stream_position\":\""+currentStreamPosition+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category1+"\","
				+ "\"ref_id\":\""+doc1.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type1+"\"}]}";
		
		timeInUUID = UUIDs.timeBased();
		statusCode = 204;
		given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).then().statusCode(statusCode);
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(currentStreamPosition));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));

			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//2
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc1.getFieldValue("uuid")));		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"2");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"','"+ref_type1+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//2
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", doc1.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		
		setValueInCQLQuery("profile_id", null);
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", doc1.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/********************************************************/		
				
		//DELETE Recent
		log.info("Verifying DELETE Recents without query parameters");
		statusCode = 204;
		
		Map<String,Object> mapOfQueryParam = new TreeMap<String,Object>();
		mapOfQueryParam.put("profile_id", profileId);
		
		Map<String,Object> mapOfHeaderParam = new TreeMap<String,Object>();
		mapOfHeaderParam.put("accept", "application/json");
		mapOfHeaderParam.put("User-Agent", USER_AGENT);
				
		getAPIResponse("http://"+config.getProperty("QA_PLAT_PRF")+":"+config.getProperty("QA_PLAT_PRF_PORT")+config.getProperty("delete.backend.recents.v1"), HttpMethodType.DELETE, null, mapOfHeaderParam, null, null, mapOfQueryParam, "application/json", null, statusCode, true, "Backend Delete Token");
		
		log.info("/******************Verification completed******************/");
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"0");
		
		keySpace = "personalization_recents";
		dbQuery = "select count(*) as countOfRows from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
				
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"2");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"','"+ref_type1+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, false);
		
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ?  AND ref_type IN('"+ref_type+"','"+ref_type1+"') AND  ref_id IN('"+String.valueOf(doc.getFieldValue("uuid"))+"','"+String.valueOf(doc1.getFieldValue("uuid"))+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"0");
		
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type IN('"+ref_type+"','"+ref_type1+"') AND  ref_id IN('"+String.valueOf(doc.getFieldValue("uuid"))+"','"+String.valueOf(doc1.getFieldValue("uuid"))+"') AND profile_id= ?";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/********************************************************/		

		//DELETE Recent
		log.info("Verifying DELETE Recents without query parameters");
		statusCode = 204;
		 given().queryParam("profileId", profileId)
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.delete(config.getProperty("delete.backend.recents.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
	}
	
	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * PLAT_Mobi_Recents_Service_GetRecentHistory_001
	 * PLAT_Mobi_Recents_Service_GetRecentHistory_008
	 * PLAT_Mobi_Recents_Service_GetRecentHistory_010
	 * PLAT_Mobi_Recents_Service_GetRecentHistory_011
	 * PLAT_Mobi_Recents_Service_GetRecentHistory_012
	 * PLAT_Mobi_Recents_Service_GetRecentHistory_013
	 * PLAT_Mobi_Recents_Service_GetRecentHistory_014
	 * 
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557515
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557522
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557524
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557525
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557526
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557527
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557528
	 *  
	 * @Summary
	 * Verify Get Recents History API should return response successfully with 200 status code
	 * Verify Get Recents API with invalid profile_id
	 * Verify Get Recents History API without recent_id_list
	 * Verify Get Recents API with null Session Id 'x-mobitv-sid: '
	 * Verify Get Recents API with invalid Session Id 'x-mobitv-sid: xyz'
	 * Verify Get Recents API without auth header 'Authorization: Bearer '
	 * Verify Get Recents API with invalid auth header 'Authorization: Bearer blahblah'
	 */
	
	@Test
	public void testPLAT_Mobi_Recents_Service_GetRecentHistory_001_008_010_to_014() throws Exception
	{		
		String solrQuery = "type_ssi:vod AND offer_ids_ssmi:*";

		//Fetching valid vod doc
		ref_type = "vod";
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for vod: "+doc.getFieldValue("uuid"));
		List<Object> listOfCategories = (ArrayList<Object>)(doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size()==0 ? "comedy" : String.valueOf(listOfCategories.get(0)));	
				
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+duration+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\""+doc.getFieldValue("uuid")+"\","
				+ "\"ref_type\":\""+ref_type+"\"}]}";
		
		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).getStatusCode();
		
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, String.valueOf(doc.getFieldValue("uuid")));
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(duration));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));

			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type= ? AND ref_id= ?";
				
			//1
		setValueInCQLQuery("profile_id",null);
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", doc.getFieldValue("uuid"));
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/******************************************************/
		
		//GET Recent with null details
		log.info("Verifying GET Recents History without session details");
		statusCode = 200;
		given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("recent_id_list", ref_type+"|"+String.valueOf(doc.getFieldValue("uuid")))
				.header("accept", "application/json").and()
				.header("x-mobitv-sid", "").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.history.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
		
		/******************************************************/
		
		//GET Recent with invalid details
		log.info("Verifying GET Recents with invalid session details");
		statusCode = 200;
		given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("recent_id_list", ref_type+"|"+String.valueOf(doc.getFieldValue("uuid")))
				.header("accept", "application/json").and()
				.header("x-mobitv-sid", "xyz").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.history.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
		
		//GET Recent without recent_id_list details
		log.info("Verifying GET Recents without recent_id_list details");
		statusCode = 400;
		given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.header("accept", "application/json").and()
				.header("x-mobitv-sid", "xyz").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.history.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
		
		//GET Recent for invalid profile ID
		log.info("Verifying GET Recents for invalid Profile ID");
		statusCode = 403;
		given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", "1111111111111")
				.queryParam("recent_id_list", ref_type+"|"+String.valueOf(doc.getFieldValue("uuid")))
				.header("accept", "application/json").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.history.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
		
		//GET Recent without Authorization
		log.info("Verifying GET Recents without Authorization");
		statusCode = 403;
		given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("recent_id_list", ref_type+"|"+String.valueOf(doc.getFieldValue("uuid")))
				.header("accept", "application/json").and()
				.header("Authorization", "Bearer ").and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.history.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
		
		//GET Recent with invalid Authorization
		log.info("Verifying GET Recents with invalid Authorization");
		statusCode = 403;
		given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("recent_id_list", ref_type+"|"+String.valueOf(doc.getFieldValue("uuid")))
				.header("accept", "application/json").and()
				.header("Authorization", "Bearer BlahBlah").and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.history.v1")).then()
				.statusCode(statusCode);
		log.info("/******************Verification completed******************/");
	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * PLAT_Mobi_Recents_Service_GetRecentHistory_002
	 * PLAT_Mobi_Recents_Service_GetRecentHistory_003
	 * PLAT_Mobi_Recents_Service_GetRecentHistory_006
	 * PLAT_Mobi_Recents_Service_GetRecentHistory_007
	 *  
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557516
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557517
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557520
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557521
	 *  
	 * @Summary
	 * Verify Get Recents History API by ref_type as vod and ref_id in recent_id_list as query param in request such as vod|20
	 * Verify Get Recents History API by ref_type as program and ref_id in recent_id_list as query param in request such as program|20
	 * Verify Get Recents History API by multiple ref_type|ref_ids in recent_id_list in request such as vod|20,program|100
	 * Verify Get Recents History API by non existing ref_ids in recent_id_list in request
	 */
	
	@Test
	public void testPLAT_Mobi_Recents_Service_GetRecentHistory_002_003_006_007() throws Exception
	{		
		String solrQuery = "type_ssi:vod AND offer_ids_ssmi:*";

		//Fetching valid vod doc
		ref_type = "vod";
		String ref_type1 = "program";
		
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for vod: 1"+doc.getFieldValue("uuid")+"1");
		log.info("Verifying POST Recents for program: 2"+doc.getFieldValue("uuid")+"2");
		List<Object> listOfCategories = (ArrayList<Object>)(doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size()==0 ? "comedy" : String.valueOf(listOfCategories.get(0)));	
				
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+duration+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\"1"+doc.getFieldValue("uuid")+"1\","
				+ "\"ref_type\":\""+ref_type+"\"},{"
				+ "\"current_stream_position\":\""+duration+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\"2"+doc.getFieldValue("uuid")+"2\","
				+ "\"ref_type\":\""+ref_type1+"\"}]}";
		
		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).getStatusCode();
		
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, "1"+String.valueOf(doc.getFieldValue("uuid"))+"1");
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(duration));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));

			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, "2"+String.valueOf(doc.getFieldValue("uuid"))+"2");
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"2");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type+"','"+ref_type1+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		
			//1
		setValueInCQLQuery("ref_type", ref_type);
		setValueInCQLQuery("ref_id", "1"+doc.getFieldValue("uuid")+"1");
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//2
		setValueInCQLQuery("ref_type", ref_type1);
		setValueInCQLQuery("ref_id", "2"+doc.getFieldValue("uuid")+"2");
		verifyCassandraDBEntries(keySpace, dbQuery, true);
				
		//Verification DB Table:recents_profile	
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"2");
		
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type IN('"+ref_type+"','"+ref_type1+"') AND  ref_id IN('1"+String.valueOf(doc.getFieldValue("uuid"))+"1','2"+String.valueOf(doc.getFieldValue("uuid"))+"2') AND profile_id= ?";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/******************************************************/
		
		//GET Recent hitory with details
		log.info("Verifying GET Recents History details for ref_type:"+ref_type);
		RestAssured.defaultParser = Parser.XML;
		statusCode = 200;
		RecentList response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("recent_id_list", ref_type+"|1"+String.valueOf(doc.getFieldValue("uuid"))+"1")
				.header("accept", "application/xml").and()
				.header("x-mobitv-sid", "").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.history.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);
		
		List<RecentListItem> listOfRecentItems = response.getRecentlistItems();
		Assert.assertTrue("Recent item list must contain only one item", (listOfRecentItems != null) && (listOfRecentItems.size() == 1));
		
		RecentListItem item = listOfRecentItems.get(0);
		log.info("Ref_ID: "+item.getRefId()+" Ref_Type: "+item.getRefType());
		
		Assert.assertTrue("Expected item ref_id:1"+String.valueOf(doc.getFieldValue("uuid"))+"1 with ref_type:"+ref_type+" not found", (item.getRefId().equalsIgnoreCase("1"+String.valueOf(doc.getFieldValue("uuid"))+"1")) && item.getRefType().equalsIgnoreCase(ref_type));
		log.info("/******************Verification completed******************/");
		
		//GET Recent hitory with details
		log.info("Verifying GET Recents History details for ref_type:"+ref_type1);
		statusCode = 200;
		response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("recent_id_list", ref_type1+"|2"+String.valueOf(doc.getFieldValue("uuid"))+"2")
				.header("accept", "application/xml").and()
				.header("x-mobitv-sid", "").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.history.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);
				
		listOfRecentItems = response.getRecentlistItems();
		Assert.assertTrue("Recent item list must contain only one item", (listOfRecentItems != null) && (listOfRecentItems.size() == 1));
		
		item = listOfRecentItems.get(0);
		log.info("Ref_ID: "+item.getRefId()+" Ref_Type: "+item.getRefType());
		
		Assert.assertTrue("Expected item ref_id:2"+String.valueOf(doc.getFieldValue("uuid"))+"2 with ref_type:"+ref_type1+" not found", (item.getRefId().equalsIgnoreCase("2"+String.valueOf(doc.getFieldValue("uuid"))+"2")) && item.getRefType().equalsIgnoreCase(ref_type1));
		log.info("/******************Verification completed******************/");
		
		//GET Recent hitory with multiple ref_type|ref_id pair details
		log.info("Verifying GET Recents History details for ref_type:["+ref_type+","+ref_type1+"]");
		statusCode = 200;
		response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("recent_id_list", ref_type+"|1"+String.valueOf(doc.getFieldValue("uuid"))+"1,"+ref_type1+"|2"+String.valueOf(doc.getFieldValue("uuid"))+"2")
				.header("accept", "application/xml").and()
				.header("x-mobitv-sid", "").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.history.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);
				
		listOfRecentItems = response.getRecentlistItems();
		Assert.assertTrue("Recent item list must contain only two items", (listOfRecentItems != null) && (listOfRecentItems.size() == 2));
		
		item = listOfRecentItems.get(0);
		log.info("Ref_ID: "+item.getRefId()+" Ref_Type: "+item.getRefType());
		item = listOfRecentItems.get(1);
		log.info("Ref_ID: "+item.getRefId()+" Ref_Type: "+item.getRefType());
		
		log.info("/******************Verification completed******************/");
		
		//GET Recent hitory with details
		log.info("Verifying GET Recents History details for ref_type:"+ref_type+" and invalid ref_id");
		statusCode = 200;
		response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("recent_id_list", ref_type+"|101001010101010101010")
				.header("accept", "application/xml").and()
				.header("x-mobitv-sid", "").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.history.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);
				
		listOfRecentItems = response.getRecentlistItems();
		Assert.assertTrue("Recent item list must be empty", (listOfRecentItems == null) | (listOfRecentItems.size() == 0));
		
		log.info("/******************Verification completed******************/");
		
	}

	/**
	 * @test.type
	 * functional
	 * 
	 * @TestCaseID
	 * PLAT_Mobi_Recents_Service_GetRecentHistory_004
	 * PLAT_Mobi_Recents_Service_GetRecentHistory_005
	 *  
	 * @Testraillink
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557518
	 * https://testrail.mobitv.corp/index.php?/cases/view/1557519
	 *  
	 * @Summary
	 * Verify Get Recents History API by ref_type as catchup and ref_id in recent_id_list as query param in request such as catchup|20
	 * Verify Get Recents History API by ref_type as recording and ref_id in recent_id_list as query param in request such as recording|20
	 */
	
	@Test
	public void testPLAT_Mobi_Recents_Service_GetRecentHistory_004_005() throws Exception
	{		
		String solrQuery = "type_ssi:program AND offer_ids_ssmi:*";

		//Fetching valid vod doc
		String ref_type2 = "catchup";
		String ref_type3 = "recording";
		
		SolrDocument doc = getValidSolrDocID(solrQuery);
		log.info("Verifying POST Recents for "+ref_type2+": 3"+doc.getFieldValue("uuid")+"3");
		log.info("Verifying POST Recents for "+ref_type3+": 4"+doc.getFieldValue("uuid")+"4");
		List<Object> listOfCategories = (ArrayList<Object>)(doc.getFieldValues("genre_list_ssmi"));
		category = (listOfCategories == null || listOfCategories.size()==0 ? "comedy" : String.valueOf(listOfCategories.get(0)));	
				
		//Generating Profile ID
		grantType = (grantType.equalsIgnoreCase("gateway_header"))?grantType:"gateway_header";
		setUp();
		String authToken = "Bearer " + accessToken;
		
		//POST Recent
		String data = "{\"recentlist_items\":[{"
				+ "\"current_stream_position\":\""+duration+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\"3"+doc.getFieldValue("uuid")+"3\","
				+ "\"ref_type\":\""+ref_type2+"\"},{"
				+ "\"current_stream_position\":\""+duration+"\","
				+ "\"duration\":\""+duration+"\","
				+ "\"category\":\""+category+"\","
				+ "\"ref_id\":\"4"+doc.getFieldValue("uuid")+"4\","
				+ "\"ref_type\":\""+ref_type3+"\"}]}";
		
		timeInUUID = UUIDs.timeBased();
		int actualStatus = given().
				pathParam("carrier", carrier).
		        pathParam("product", product).
		        pathParam("version", version).
		        pathParam("profileId", profileId).
		        contentType("application/json").
		        body(data).
		        header("accept","application/json").and().header("Authorization",authToken).and().header("User-Agent", USER_AGENT).
		        when(). 
		        post(config.getProperty("post.recents.v1")).getStatusCode();
		
		statusCode = 204;
		log.info("Response code: "+actualStatus);
		Assert.assertEquals("Status code not as expected", statusCode, actualStatus);
		
		/*****************************************************************************/
		//Verification DB Table:profile_recents_all
		setDetailsIntoRecordVerificationMap(1, "created_time", ColumnTypes.UUID, timeInUUID.toString());
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, "3"+String.valueOf(doc.getFieldValue("uuid"))+"3");
		setDetailsIntoRecordVerificationMap(1, "current_stream_position", ColumnTypes.STRING, String.valueOf(duration));
		setDetailsIntoRecordVerificationMap(1, "duration", ColumnTypes.STRING, String.valueOf(duration));

			//1
		keySpace = "personalization_recents";
		dbQuery = "select * from profile_recents_all where profile_id= ?";
		setValueInCQLQuery("profile_id", profileId);		
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		setDetailsIntoRecordVerificationMap(1, "ref_id", ColumnTypes.STRING, "4"+String.valueOf(doc.getFieldValue("uuid"))+"4");
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_reftype
		setDetailsIntoRecordVerificationMap(-1,"",null,"");
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"2");
		
		dbQuery = "select count(*) as countOfRows from profile_recents_reftype where profile_id= ? AND ref_type IN('"+ref_type2+"','"+ref_type3+"')";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:profile_recents_history	
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"1");
		dbQuery = "select count(*) as countOfRows from profile_recents_history where profile_id= ? AND ref_type= ? AND ref_id= ?";
		
			//1
		setValueInCQLQuery("ref_type", ref_type2);
		setValueInCQLQuery("ref_id", "3"+doc.getFieldValue("uuid")+"3");
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
			//2
		setValueInCQLQuery("ref_type", ref_type3);
		setValueInCQLQuery("ref_id", "4"+doc.getFieldValue("uuid")+"4");
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		
		//Verification DB Table:recents_profile	
		setDetailsIntoRecordVerificationMap(1, "countOfRows", ColumnTypes.LONG,"2");
		
		dbQuery = "select count(*) as countOfRows from recents_profile where ref_type IN('"+ref_type2+"','"+ref_type3+"') AND  ref_id IN('3"+String.valueOf(doc.getFieldValue("uuid"))+"3','4"+String.valueOf(doc.getFieldValue("uuid"))+"4') AND profile_id= ?";
		verifyCassandraDBEntries(keySpace, dbQuery, true);
		/******************************************************/
		
		//GET Recent hitory with details
		log.info("Verifying GET Recents History details for ref_type:"+ref_type2);
		RestAssured.defaultParser = Parser.XML;
		statusCode = 200;
		RecentList response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("recent_id_list", ref_type2+"|3"+String.valueOf(doc.getFieldValue("uuid"))+"3")
				.header("accept", "application/xml").and()
				.header("x-mobitv-sid", "").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.history.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);
				
		List<RecentListItem> listOfRecentItems = response.getRecentlistItems();
		Assert.assertTrue("Recent item list must contain only one item", (listOfRecentItems != null) && (listOfRecentItems.size() == 1));
		
		RecentListItem item = listOfRecentItems.get(0);
		log.info("Ref_ID: "+item.getRefId()+" Ref_Type: "+item.getRefType());
		
		Assert.assertTrue("Expected item ref_id:3"+String.valueOf(doc.getFieldValue("uuid"))+"3 with ref_type:"+ref_type2+" not found", (item.getRefId().equalsIgnoreCase("3"+String.valueOf(doc.getFieldValue("uuid"))+"3")) && item.getRefType().equalsIgnoreCase(ref_type2));
		log.info("/******************Verification completed******************/");
		
		//GET Recent hitory with details
		log.info("Verifying GET Recents History details for ref_type:"+ref_type3);
		statusCode = 200;
		response = given().pathParam("carrier", carrier)
				.pathParam("product", product)
				.pathParam("version", version)
				.pathParam("profileId", profileId)
				.queryParam("recent_id_list", ref_type3+"|4"+String.valueOf(doc.getFieldValue("uuid"))+"4")
				.header("accept", "application/xml").and()
				.header("x-mobitv-sid", "").and()
				.header("Authorization", authToken).and()
				.header("User-Agent", USER_AGENT).when()
				.get(config.getProperty("get.recents.history.v1")).then()
				.statusCode(statusCode).and().extract().as(RecentList.class);
				
		listOfRecentItems = response.getRecentlistItems();
		Assert.assertTrue("Recent item list must contain only one item", (listOfRecentItems != null) && (listOfRecentItems.size() == 1));
		
		item = listOfRecentItems.get(0);
		log.info("Ref_ID: "+item.getRefId()+" Ref_Type: "+item.getRefType());
		
		Assert.assertTrue("Expected item ref_id:4"+String.valueOf(doc.getFieldValue("uuid"))+"4 with ref_type:"+ref_type3+" not found", (item.getRefId().equalsIgnoreCase("4"+String.valueOf(doc.getFieldValue("uuid"))+"4")) && item.getRefType().equalsIgnoreCase(ref_type3));
		log.info("/******************Verification completed******************/");
	}

}

